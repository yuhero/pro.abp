﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Repositories;

namespace Volo.Abp.LanguageManagement
{
	public interface ILanguageTextRepository : IReadOnlyBasicRepository<LanguageText, Guid>, IReadOnlyBasicRepository<LanguageText>, IBasicRepository<LanguageText>, IBasicRepository<LanguageText, Guid>, IRepository
	{
		List<LanguageText> GetList(string resourceName, string cultureName);
	}
}
