﻿using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    public class Language : FullAuditedAggregateRoot<Guid>, ILanguageInfo
	{
		public virtual string CultureName { get; protected set; }

		public virtual string UiCultureName { get; protected set; }

		public virtual string DisplayName { get; protected set; }

		public virtual string FlagIcon { get; set; }

		public virtual bool IsEnabled { get; set; }

		public Language(Guid id, string cultureName, string uiCultureName = null, string displayName = null, string flagIcon = null, bool isEnabled = true)
		{
			this.Id = id;
			this.ChangeCultureInternal(cultureName, uiCultureName, displayName);
			this.FlagIcon = flagIcon;
			this.IsEnabled = isEnabled;
		}

		public virtual void ChangeCulture(string cultureName, string uiCultureName = null, string displayName = null)
		{
			this.ChangeCultureInternal(cultureName, uiCultureName, displayName);
		}

		protected virtual void ChangeCultureInternal(string cultureName, string uiCultureName, string displayName)
		{
			this.CultureName = Check.NotNullOrWhiteSpace(cultureName, "cultureName", int.MaxValue, 0);
			this.UiCultureName = ((!uiCultureName.IsNullOrWhiteSpace()) ? uiCultureName : cultureName);
			this.DisplayName = ((!displayName.IsNullOrWhiteSpace()) ? displayName : cultureName);
		}

		public virtual void SetDisplayName(string displayName)
		{
			this.DisplayName = displayName;
		}
	}
}
