﻿using AutoMapper;
using Volo.Abp.AutoMapper;
using Volo.Abp.LanguageManagement.Dto;

namespace Volo.Abp.LanguageManagement
{
	public class LanguageManagementApplicationAutoMapperProfile : Profile
	{
		public LanguageManagementApplicationAutoMapperProfile()
		{
			base.CreateMap<Language, LanguageDto>().MapExtraProperties<Language, LanguageDto>()
				.Ignore<Language, LanguageDto, bool>(x => x.IsDefaultLanguage);
		}
	}
}
