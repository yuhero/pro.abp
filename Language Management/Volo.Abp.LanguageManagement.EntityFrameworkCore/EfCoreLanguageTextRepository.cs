﻿using System;
using System.Collections.Generic;
using System.Linq;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Volo.Abp.LanguageManagement.EntityFrameworkCore
{
    public class EfCoreLanguageTextRepository : EfCoreRepository<ILanguageManagementDbContext, LanguageText, Guid>, IReadOnlyBasicRepository<LanguageText, Guid>, IReadOnlyBasicRepository<LanguageText>, IBasicRepository<LanguageText>, IBasicRepository<LanguageText, Guid>, IRepository, ILanguageTextRepository
	{
		public EfCoreLanguageTextRepository(IDbContextProvider<ILanguageManagementDbContext> dbContextProvider)
			: base(dbContextProvider)
		{
		}

		public virtual List<LanguageText> GetList(string resourceName, string cultureName)
		{
			return this.DbSet.Where(x => x.ResourceName.Equals(resourceName)).Where(x => x.CultureName.Equals(cultureName)).ToList<LanguageText>();
		}
	}
}
