﻿namespace Volo.Abp.LanguageManagement
{
    public static class LanguageTextConsts
	{
		public static int MaxResourceNameLength { get; set; }

		public static int MaxKeyNameLength { get; set; }

		public static int MaxValueLength { get; set; }

		public static int MaxCultureNameLength;

		static LanguageTextConsts()
		{
			MaxResourceNameLength = 128;
			MaxKeyNameLength = 512;
			MaxValueLength = 67108864;
			MaxCultureNameLength = LanguageConsts.MaxCultureNameLength;
		}
	}
}
