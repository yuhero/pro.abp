﻿using System;

namespace Volo.Abp.Account.Public.Web.Areas.Account.Controllers.Models
{
	public enum LoginResultType : byte
	{
		Success = 1,
		InvalidUserNameOrPassword,
		NotAllowed,
		LockedOut,
		RequiresTwoFactor
	}
}
