﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;
using Volo.Abp;

public static class LicenseChecker
{
	public static void Check<T>(ApplicationInitializationContext context)
	{
		bool isDevelopment = string.Equals(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"), "Development", StringComparison.OrdinalIgnoreCase);
		Check<T>(context, isDevelopment);
	}

	public static void Check<T>(ApplicationInitializationContext context, bool isDevelopment)
	{
		var configuration = context.ServiceProvider.GetRequiredService<IConfiguration>();
		var logger = context.ServiceProvider.GetRequiredService<ILogger<T>>();
		Check<T>(configuration["AbpLicenseCode"], logger, isDevelopment);
	}

	public static void Check<T>(string organizationLicenseCode, ILogger logger = null, bool isDevelopment = false)
	{
		Check(Assembly.GetAssembly(typeof(T)), organizationLicenseCode, logger, isDevelopment);
	}

	public static void Check(Assembly assembly, string organizationLicenseCode, ILogger logger = null, bool isDevelopment = false)
	{
		
	}
}
