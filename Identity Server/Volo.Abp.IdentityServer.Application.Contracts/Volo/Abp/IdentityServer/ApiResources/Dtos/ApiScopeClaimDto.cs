﻿using System;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class ApiScopeClaimDto
	{
		public Guid ApiResourceId { get; set; }

		public string Name { get; set; }

		public string Type { get; set; }
	}
}
