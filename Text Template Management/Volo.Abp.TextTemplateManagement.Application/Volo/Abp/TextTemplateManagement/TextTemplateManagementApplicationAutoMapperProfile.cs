﻿using AutoMapper;
using Volo.Abp.AutoMapper;
using Volo.Abp.TextTemplateManagement.TextTemplates;
using Volo.Abp.TextTemplating;

namespace Volo.Abp.TextTemplateManagement
{
    public class TextTemplateManagementApplicationAutoMapperProfile : Profile
	{
		public TextTemplateManagementApplicationAutoMapperProfile()
		{
			base.CreateMap<TemplateDefinition, TemplateDefinitionDto>().Ignore<TemplateDefinition, TemplateDefinitionDto, string>(x => x.DisplayName);
			base.CreateMap<TextTemplateContent, TextTemplateContentDto>();
		}
	}
}
