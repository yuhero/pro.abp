﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.TextTemplateManagement
{
    [DependsOn(
		typeof(TextTemplateManagementDomainSharedModule),
		typeof(AbpDddApplicationContractsModule),
		typeof(AbpAuthorizationModule)
	)]
	public class TextTemplateManagementApplicationContractsModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options => options.FileSets.AddEmbedded<TextTemplateManagementApplicationContractsModule>());
		}
	}
}
