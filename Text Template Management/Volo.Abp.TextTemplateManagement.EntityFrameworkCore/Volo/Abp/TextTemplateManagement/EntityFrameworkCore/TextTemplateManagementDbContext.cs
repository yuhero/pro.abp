﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.TextTemplateManagement.TextTemplates;

namespace Volo.Abp.TextTemplateManagement.EntityFrameworkCore
{
    [ConnectionStringName("TextTemplateManagement")]
	public class TextTemplateManagementDbContext : AbpDbContext<TextTemplateManagementDbContext>, ITextTemplateManagementDbContext, IInfrastructure<IServiceProvider>, IResettableService, IDbContextPoolable, IDbSetCache, IDbContextDependencies, IDisposable, IEfCoreDbContext
	{
		public DbSet<TextTemplateContent> TextTemplateContents { get; set; }

		public TextTemplateManagementDbContext(DbContextOptions<TextTemplateManagementDbContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.ConfigureTextTemplateManagement();
		}
	}
}
