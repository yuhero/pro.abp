﻿using System;

namespace Volo.Payment.Requests
{
	public enum PaymentRequestState
	{
		Waiting,
		Completed,
		Failed
	}
}
