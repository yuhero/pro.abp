﻿using System;
using Volo.Abp.Domain.Repositories;

namespace Volo.Payment.Requests
{
	public interface IPaymentRequestRepository : IReadOnlyBasicRepository<PaymentRequest, Guid>, IReadOnlyBasicRepository<PaymentRequest>, IBasicRepository<PaymentRequest>, IBasicRepository<PaymentRequest, Guid>, IRepository
	{
	}
}
